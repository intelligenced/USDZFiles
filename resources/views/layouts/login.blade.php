<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ARFiles.io - Share your AR!</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
  </head>
  <body>


        <nav class="navbar is-light has-shadow is-spaced">
        <div class="navbar-brand">
      
      <!-- Testing comments, this will work hrefo. 
      
                 <a class="navbar-item" href="/">
                       <img src="https://bulma.io/images/bulma-logo.png" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
      
                 </a>
      
      
      -->
      
        <a class="navbar-item" href="/">
                     <h1 class="title"> <b> AR Files.io  </b></h1>
      
                 </a>
      
          <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      
        <div id="navbarExampleTransparentExample" class="navbar-menu">
          <div class="navbar-end">
             {{-- <a class="navbar-item" href="/">Home</a>
             <a class="navbar-item" href="/about">About</a> --}}

          </div>
      
      
        </div>
      </nav>
      
      
      
      @yield('content')
      


      <footer class="footer">
            <div class="container">
              <div class="content has-text-centered">
                <p>
                  <strong>Bulma</strong> by <a href="https://jgthms.com">Jeremy Thomas</a>. The source code is licensed
                  <a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
                  is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY NC SA 4.0</a>.
                </p>
              </div>
            </div>
          </footer>


            
     
  </body>
</html>