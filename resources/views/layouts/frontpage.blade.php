<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ARFiles.io - Share your AR!</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
  </head>
  <body>


        <nav class="navbar is-light has-shadow is-spaced">
        <div class="navbar-brand">
      
      <!-- Testing comments, this will work hrefo. 
      
                 <a class="navbar-item" href="/">
                       <img src="https://bulma.io/images/bulma-logo.png" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
      
                 </a>
      
      
      -->
      
        <a class="navbar-item" href="/">
                     <h1 class="title"> <b> AR Files.io  </b></h1>
      
                 </a>
      
          <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      
        <div id="navbarExampleTransparentExample" class="navbar-menu">
          <div class="navbar-end">
             {{-- <a class="navbar-item" href="/">Home</a>
             <a class="navbar-item" href="/about">About</a> --}}

             @guest
             @else
             <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link" href="/documentation/overview/start/">
                  {{ Auth::user()->name }} 
                </a>
                <div class="navbar-dropdown is-boxed">
                  <a class="navbar-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>

                  <a class="navbar-item" href="https://bulma.io/documentation/modifiers/syntax/">
                    Modifiers
                  </a>
                  <a class="navbar-item" href="https://bulma.io/documentation/columns/basics/">
                    Columns
                  </a>
                  <a class="navbar-item" href="https://bulma.io/documentation/layout/container/">
                    Layout
                  </a>
                  <a class="navbar-item" href="https://bulma.io/documentation/form/general/">
                    Form
                  </a>
                  <hr class="navbar-divider">
                  <a class="navbar-item" href="https://bulma.io/documentation/elements/box/">
                    Elements
                  </a>
                  <a class="navbar-item is-active" href="https://bulma.io/documentation/components/breadcrumb/">
                    Components
                  </a>
                </div>
              </div>
              @endif

             <a class="button is-link" href="/share">Share your  AR File</a>




          </div>
      
      
        </div>
      </nav>
      <section class="section ">

        
          <div class="columns">
            <aside class="column is-2 ">
              <nav class="menu">
      
      
                      
                <p class="menu-label">
                  Categories
                </p>
                <ul class="menu-list">
                      <li><a>All</a></li>
      
                  <li><a>Characters</a></li>
                  <li><a>Animals</a></li>
                  <li><a>Vehicles</a></li>
                  <li><a>Architecture</a></li>
                  <li><a>Furniture</a></li>
                  <li><a>Anahrefmy</a></li>
                  <li><a>Landscape</a></li>
                  <li><a>Technology</a></li>
                  <li><a>Misc</a></li>
        
                </ul>
        
        
              </nav>
            </aside>
            
            <main class="column ">
     
                @yield('content')

        
            </main>
          </div>
        </section>
      
      
      


      <footer class="footer">
            <div class="container">
              <div class="content has-text-centered">
                <p>
                  <strong>Bulma</strong> by <a href="https://jgthms.com">Jeremy Thomas</a>. The source code is licensed
                  <a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
                  is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY NC SA 4.0</a>.
                </p>
              </div>
            </div>
          </footer>


            
     <script>

       (function() {

// var input = document.getElementById( 'usdz-file' );
// var infoArea = document.getElementById( 'usdz-filename' );


showFileNameHelper('usdz-file', 'usdz-filename');
showFileNameHelper('thumbnail-file', 'thumbnail-filename');

function showFileNameHelper(inputName, placeholder){
  var input = document.getElementById( inputName);
  var infoArea = document.getElementById(placeholder);
  input.addEventListener( 'change', function(event){
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea.textContent = 'File name: ' + fileName;
  });
}


//input.addEventListener( 'change', showFileName );
   // your page initialization code here
   // the DOM will be available here

   console.log('test')
   //input.addEventListener( 'change', showFileName );


   function showFileName( event ) {
  
  // the change event gives us the input it occurred in 
  var input = event.srcElement;
  
  // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
  var fileName = input.files[0].name;
  
  // use fileName however fits your app best, i.e. add it into a div
  infoArea.textContent = 'File name: ' + fileName;
}


})();



      </script>
  </body>
</html>