@extends('layouts.login')

@section('content')


<section class="section has-background-light " >
    <div class="container">
        <div class="columns">
            <div class="column is-half">
              
              <div class="is-size-2"> Need an account?</div>
              <div class="is-size-4"> <a href="/register"> Create your account now </a></div>

            
            </div>
            <div class="column">
                <div class="is-size-2"> Login  </div>
              <br>
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf

                <div class="field">
                    <label class="label">Email</label>
                    <div class="control  ">
                      <input class="input {{ $errors->has('email') ? '  is-danger' : '' }} " id="email" name="email" type="email"  value="{{ old('email') }}">
                    </div>
                    @if ($errors->has('email'))
                    <p class="help is-danger">{{ $errors->first('email') }}</p>
                    @endif
                  </div>


                  <div class="field">
                      <label class="label">Password</label>
                      <div class="control  ">
                        <input class="input {{ $errors->has('password') ? '  is-danger' : '' }} " id="password" name="password" type="password"  >
                      </div>
                      @if ($errors->has('email'))
                      <p class="help is-danger">{{ $errors->first('password') }}</p>
                      @endif
                    </div>

                    <div class="field">
                        <div class="control">
                          <label class="checkbox">

                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}

                          </label>
                        </div>
                      </div>

                      


                      <div class="field">
                          <div class="control">
                            <button class="button is-link">Login</button>
                          </div>
        
                        </div>

                        <a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                      </form>

            </div>
          </div>



    </div>

</section>



@endsection
