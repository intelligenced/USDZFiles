@extends('layouts.login')

@section('content')


<section class="section has-background-light " >
        <div class="container">
            <div class="columns">
                <div class="column is-half">
                  
                  <div class="is-size-2"> Have an account?</div>
                  <div class="is-size-4"> <a href="/login"> Login here </a></div>
    
                
                </div>
                <div class="column">
                    <div class="is-size-2"> Create your account  </div>
                  <br>
                  <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf


                        <div class="field">
                                <label class="label">Name</label>
                                <div class="control  ">
                                  <input class="input {{ $errors->has('name') ? '  is-danger' : '' }} " id="name" name="name" type="name"  value="{{ old('name') }}">
                                </div>
                                @if ($errors->has('name'))
                                <p class="help is-danger">{{ $errors->first('name') }}</p>
                                @endif
                              </div>


    
                    <div class="field">
                        <label class="label">Email</label>
                        <div class="control  ">
                          <input class="input {{ $errors->has('email') ? '  is-danger' : '' }} " id="email" name="email" type="email"  value="{{ old('email') }}">
                        </div>
                        @if ($errors->has('email'))
                        <p class="help is-danger">{{ $errors->first('email') }}</p>
                        @endif
                      </div>
    
    
                      <div class="field">
                          <label class="label">Password</label>
                          <div class="control  ">
                            <input class="input {{ $errors->has('password') ? '  is-danger' : '' }} " id="password" name="password" type="password"  >
                          </div>
                          @if ($errors->has('password'))
                          <p class="help is-danger">{{ $errors->first('password') }}</p>
                          @endif
                        </div>


                        <div class="field">
                                <label class="label"> Confirm Password</label>
                                <div class="control  ">

                                        <input class="input {{ $errors->has('password') ? '  is-danger' : '' }} " id="password-confirm"  name="password_confirmation" type="password"  >

                                </div>

                              </div>
          
    

                          
    
    
                          <div class="field">
                              <div class="control">
                                <button class="button is-link">Sign Up</button>
                              </div>
            
                            </div>
    
                          </form>
    
                </div>
              </div>
    
    
    
        </div>
    
    </section>


@endsection
