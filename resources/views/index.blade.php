@extends('layouts.frontpage')
@section('content')
 
      
        
<div class="is-size-1 ">
    Discover, Explore and Share USDZ!
</div>
<br>
<div class="is-size-3">

What is USDZ?
</div>
<p class="is-size-5"> 
USDZ is a new file format co-created by Apple and Pixar based on Pixar's Universal Scene Description technology. The file format is a zip archive that collects the various bits of information needed for augmented reality experiences, making it possible href easily transfer that information between apps and devices.
</p>
<br>

<div class="tabs">
<ul>
<li class="is-active"><a>What's New?</a></li>

</ul>
</div>



<div class="columns">
<div class="column is-one-fifth">

<div class="card">
<div class="card-image">
<figure class="image is-4by4">
 <img src="https://developer.apple.com/arkit/gallery/models/retrotv/retrotv.jpg" alt="Placeholder image">
</figure>
</div>
<div class="card-content">
<div class="media">
 <div class="media-left">
   <figure class="image is-48x48">
     <img src="http://www.pngmart.com/files/1/Apple-Logo-PNG-Transparent-Image.png" alt="Placeholder image">
   </figure>
 </div>
 <div class="media-content">
   <p class="title is-4">TV</p>
   <p class="subtitle is-6">@Apple</p>
 </div>
</div>

<div class="content">
. <a>@bulmaio</a>.
 <a href="#">#css</a> <a href="#">#responsive</a>
 <br>
 <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
</div>
</div>
</div> 

</div>
<div class="column is-one-fifth">
   
<div class="card">
<div class="card-image">
<figure class="image is-4by4">
 <img src="https://developer.apple.com/arkit/gallery/models/wheelbarrow/wheelbarrow.jpg" alt="Placeholder image">
</figure>
</div>
<div class="card-content">
<div class="media">
 <div class="media-left">
   <figure class="image is-48x48">
     <img src="http://www.pngmart.com/files/1/Apple-Logo-PNG-Transparent-Image.png" alt="Placeholder image">
   </figure>
 </div>
 <div class="media-content">
   <p class="title is-4">TV</p>
   <p class="subtitle is-6">@Apple</p>
 </div>
</div>

<div class="content">
. <a>@bulmaio</a>.
 <a href="#">#css</a> <a href="#">#responsive</a>
 <br>
 <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
</div>
</div>
</div> 

</div>
<div class="column is-one-fifth">

   
<div class="card">
<div class="card-image">
<figure class="image is-4by4">
 <img src="https://developer.apple.com/arkit/gallery/models/teapot/teapot.jpg" alt="Placeholder image">
</figure>
</div>
<div class="card-content">
<div class="media">
 <div class="media-left">
   <figure class="image is-48x48">
     <img src="http://www.pngmart.com/files/1/Apple-Logo-PNG-Transparent-Image.png" alt="Placeholder image">
   </figure>
 </div>
 <div class="media-content">
   <p class="title is-4">TV</p>
   <p class="subtitle is-6">@Apple</p>
 </div>
</div>

<div class="content">
. <a>@bulmaio</a>.
 <a href="#">#css</a> <a href="#">#responsive</a>
 <br>
 <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
</div>
</div>
</div> 

</div>
<div class="column is-one-fifth">

   
<div class="card">
<div class="card-image">
<figure class="image is-4by4">
 <img src="https://developer.apple.com/arkit/gallery/models/gramophone/gramophone.jpg" alt="Placeholder image">
</figure>
</div>
<div class="card-content">
<div class="media">
 <div class="media-left">
   <figure class="image is-48x48">
     <img src="http://www.pngmart.com/files/1/Apple-Logo-PNG-Transparent-Image.png" alt="Placeholder image">
   </figure>
 </div>
 <div class="media-content">
   <p class="title is-4">TV</p>
   <p class="subtitle is-6">@Apple</p>
 </div>
</div>

<div class="content">
. <a>@bulmaio</a>.
 <a href="#">#css</a> <a href="#">#responsive</a>
 <br>
 <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
</div>
</div>
</div> 
</div>



<div class="column is-one-fifth">

   
<div class="card">
<div class="card-image">
<figure class="image is-4by4">
 <img src="https://developer.apple.com/arkit/gallery/models/tulip/tulip.jpg" alt="Placeholder image">
</figure>
</div>
<div class="card-content">
<div class="media">
 <div class="media-left">
   <figure class="image is-48x48">
     <img src="http://www.pngmart.com/files/1/Apple-Logo-PNG-Transparent-Image.png" alt="Placeholder image">
   </figure>
 </div>
 <div class="media-content">
   <p class="title is-4">TV</p>
   <p class="subtitle is-6">@Apple</p>
 </div>
</div>

<div class="content">
. <a>@bulmaio</a>.
 <a href="#">#css</a> <a href="#">#responsive</a>
 <br>
 <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
</div>
</div>
</div> 
</div>






</div>


  @endsection