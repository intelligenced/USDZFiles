@extends('layouts.frontpage')
@section('content')
<nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="/">Home</a></li>
          <li class="is-active"><a href="#" aria-current="page">Submit USDZ</a></li>
        </ul>
      </nav>



    <div class="is-size-1">
    Submit USDZ
    </div>
    <div class="is-size-4">
            Upload your USDZ & Image file to showcase your USDZ with our community!
            </div>

            <br>

            <div class="column is-half">

            <div class="field">
                    <label class="label">Name</label>
                    <div class="control">
                      <input class="input" type="text" name="name">
                    </div>
                  </div>
                  
       
                  
       
                  
          
                  
                  <div class="field">
                    <label class="label">Description</label>
                    <div class="control">
                      <textarea class="textarea" name="description" ></textarea>
                    </div>
                  </div>

                  <div class="field">
                        <label class="label">Category</label>
                        <div class="control">

                          <div class="select">
                                {!! Form::select('size', ['L' => 'Large', 'S' => 'Small']) !!}

                   
                          </div>
                        </div>
                      </div>



                  <div class="field">
                        <label class="label">Thumnail Image</label>
                        <div class="control">
                                <div class="file has-name is-fullwidth">
                                        <label class="file-label">
                                          <input class="file-input" type="file" name="thumbnail" id="thumbnail-file">
                                          <span class="file-cta">
                                            <span class="file-icon">
                                              <i class="fas fa-upload"></i>
                                            </span>
                                            <span class="file-label">
                                              Choose a file…
                                            </span>
                                          </span>
                                          <span class="file-name">
                                                <div id="thumbnail-filename">

                                            Your thumbnail image
                                                </div>
                                          </span>
                                        </label>
                                      </div>
                                                           </div>
                      </div>





                  <div class="field">
                        <label class="label">.USDZ File</label>
                        <div class="control">
                                <div class="file has-name is-fullwidth">
                                        <label class="file-label">
                                          <input class="file-input file-upload"  type="file" name="usdz" id="usdz-file">
                                          <span class="file-cta">
                                            <span class="file-icon">
                                              <i class="fas fa-upload"></i>
                                            </span>
                                            <span class="file-label ">
                                              Choose a file…
                                            </span>
                                          </span>
                                          <span class="file-name file-upload-filename " >
                                              <div id="usdz-filename">
                                                    Your .usdz file

                                              </div>
                                          </span>
                                        </label>
                                      </div>
                                                           </div>
                      </div>


                      <div class="field">
                            <label class="label">Purchase Text / Link</label>
                            <div class="control">
                              <input class="input" type="text" placeholder="Text input">
                            </div>
                          </div>
                          


     
                  
                  <div class="field">
                    <div class="control">
                      <label class="checkbox">
                        <input type="checkbox">
                        I agree to the <a href="#">terms and conditions</a>
                      </label>
                    </div>
                  </div>
                  

                  
                  <div class="field is-grouped">
                    <div class="control">
                      <button class="button is-link">Submit</button>
                    </div>
                    <div class="control">
                      <button class="button is-text">Cancel</button>
                    </div>
                  </div>
                </div>
@endsection