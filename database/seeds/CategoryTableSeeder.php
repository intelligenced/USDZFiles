<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [ 'name' => 'Characters' ],
            [ 'name' => 'Animals' ],
            [ 'name' => 'Vehicles' ],
            [ 'name' => 'Architecture' ],
            [ 'name' => 'Furniture' ],
            [ 'name' => 'Landscape' ],
            [ 'name' => 'Technology' ],
            [ 'name' => 'Misc' ]
    
    );
        //
    }
}
