<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class DefaultComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = \App\Category::all();
        $view->with('categories', $categories);
    }
}